package model;

public class AcademyChoucairData {
    private String strFirstName;
    private String strLastName;
    Private String strEmail;
    Private String DateBirth;
    Private String Languages;

    public String getStrFirstName() {
        return strFirstName;
    }

    public void setStrFirstName(String strFirstName) {
        this.strFirstName = strFirstName;
    }

    public String getStrLastName() {
        return strLastName;
    }

    public void setStrLastName(String strLastName) {
        this.strLastName = strLastName;
    }

    public String getStrEmail() {
        return strEmail;
    }

    public void setStrEmail(String strEmail) {
        this.strEmail = strEmail;
    }

    private String strEmail;

    public String getDateBirth() {
        return DateBirth;
    }

    public void setDateBirth(String DateBirth) {
        this.DateBirth = DateBirth;
    }
    private String DateBirth;

    public String getLanguages() {
        return Languages;
    }

    public void setLanguages(String Languages) {
        this.Languages = Languages;
    }
    private String Languages;
}

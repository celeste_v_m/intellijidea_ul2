package co.com.choucair.certification.proyectobase.tasks;

import co.com.choucair.certification.proyectobase.userinterface.ChoucairLoginPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Join implements Task {

    private String strFirstName;
    private String strLastName;
    Private String strEmail;
    Private String DateBirth;
    Private String Languages;

    public Join(String strFirstName,String strLastName,String strEmail,String DateBirth,String Languages) {


        this.strFirstName = strFirstName;
        this.strLastName = strLastName;
        this.strEmail = strEmail;
        this.DateBirth = DateBirth;
        this.Languages = Languages;

    }

    public static Join onThePage(String strFirstName,String strLastName,String strEmail,String DateBirth,String Languages) {
        return Tasks.instrumented(Login.class, strFirstName, strLastName, strEmail, DateBirth, Languages);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(ChoucairLoginPage.JOIN_BUTTON),
                Enter.theValue(strFirstName).into(ChoucairLoginPage.INPUT_FIRSTNAME),
                Enter.theValue(strLastName).into(ChoucairLoginPage.INPUT_LASTNAME),
                Enter.theValue(strEmail).into(ChoucairLoginPage.INPUT_EMAIL),
                Enter.theValue(DateBirth).into(ChoucairLoginPage.INPUT_BIRTH),
                Enter.theValue(Languages).into(ChoucairLoginPage.INPUT_LANGUAGES),
                Click.on(ChoucairLoginPage.ENTER_BUTTON)
        );
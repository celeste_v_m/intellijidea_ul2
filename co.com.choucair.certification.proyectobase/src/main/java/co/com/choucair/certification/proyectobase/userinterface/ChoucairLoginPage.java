package co.com.choucair.certification.proyectobase.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class ChoucairLoginPage  {
    public static final Target JOIN_BUTTON = Target.the("button that shows us the form to login")
            .located(By.xpath("//div[@class='d-none d-lg-block']//strong[contains(text),'Ingresar')]"));
    public static final Target INPUT_FIRSTNAME = Target.the("where do we write the name")
            .located(By.id("username"));
    public static final Target INPUT_LASTNAME = Target.the("where do we write the last name")
            .located(By.id("password"));
    public static final Target INPUT_EMAIL = Target.the("where do we write the email")
            .located(By.id("password"));
    public static final Target INPUT_BIRTH = Target.the("where do we write the email")
            .located(By.id("password"));
    public static final Target INPUT_LANGUAGES = Target.the("where do we write the email")
            .located(By.id("password"));
    public static final Target ENTER_BUTTON = Target.the("button to confirm login")
            .located(By.xpath("//button[contains(@class,'btn btn-primary')]"));


}
